<?php

class MR_IPdetector_Model_Observer {

    /**
     * switch currency by detecting GeoIP
     *
     * @param Varien_Event_Observer $observer
     *
     * @return Magikecommerce_Magikgeoip_Model_Observer
     */
    public function currencySwitch( $observer ) {
        $helper         = Mage::helper( 'mr_ipdetector' );
        $currencyCookie = $helper->getCookie( 'currency' );

        if ( ! $currencyCookie ) {
            $countryCode = Mage::getModel( 'mr_ipdetector/geoip' )->getCurrentLocation()->getCode();
            switch ( $countryCode ) {
                case 'AU':
                    $currency = 'AUD';
                    break;
                case 'GB':
                    $currency = 'GBP';
                    break;
                case 'EU':
                    $currency = 'EUR';
                    break;
                case 'NZ':
                    $currency = 'NZD';
                    break;
                default:
                    $currency = 'USD';
            }
            Mage::app()->getStore()->setCurrentCurrencyCode( $currency );
            $helper->setCookie( 'currency', $currency, false );
        }
    }

}
