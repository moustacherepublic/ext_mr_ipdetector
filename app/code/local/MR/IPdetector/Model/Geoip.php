<?php

class MR_IPdetector_Model_Geoip extends Mage_Core_Model_Abstract {

	protected $_geoip;

	/**
	 * Opens GeoIP database
	 *
	 * @return array
	 */
	private function _openGeoIP() {
		return geoip_open( Mage::helper( 'mr_ipdetector' )->getDatabasePath(), GEOIP_STANDARD );
	}

	/**
	 * Loads location data by ip
	 *
	 * @param string $ip
	 * @param string|bool $field
	 *
	 * @return MageWorx_GeoIP_Model_Geoip
	 */
	public function load( $ip, $field = null ) {

		include_once Mage::getBaseDir() . DS . 'lib' . DS . 'GeoIP' . DS . 'geoip.inc';

		$geoip = $this->_openGeoIP();
		$data  = array( 'ip' => $ip );

		$data['code']    = geoip_country_code_by_addr( $geoip, $ip );
		$data['country'] = geoip_country_name_by_addr( $geoip, $ip );

		$this->setData( $data );
		geoip_close( $geoip );

		return $this;
	}

	/**
	 * Gets location by ip address
	 *
	 * @param string $ip
	 *
	 * @return array
	 */
	public function getLocation( $ip = null ) {
		if ( is_null( $ip ) ) {
			return $this->getCurrentLocation();
		}

		$this->load( $ip );

		return $this;
	}

	/**
	 * Return current customer loaction
	 *
	 * @return mixed
	 */
	public function getCurrentLocation() {
		$session = Mage::getSingleton( 'customer/session' );

		if ( ! $session->getCustomerLocation() || ! $session->getCustomerLocation()->getCode() ) {
			$ip = Mage::helper( 'mr_ipdetector' )->getCustomerIp();
			$this->load( $ip );
			$session->setCustomerLocation( $this );
		}

		return $session->getCustomerLocation();
	}

}
