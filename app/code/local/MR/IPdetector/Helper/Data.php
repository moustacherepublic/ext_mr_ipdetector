<?php

class MR_IPdetector_Helper_Data extends Mage_Core_Helper_Abstract {
    /**
     * Returns full path to GeoIP database
     *
     * @return string
     */
    public function getDatabasePath() {
        return Mage::getBaseDir() . DS . 'lib' . DS . 'GeoIP' . DS . 'GeoIP.dat';
    }

    /**
     * Gets customer ip
     *
     * @return string
     */
    public function getCustomerIp() {
        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif ( ! empty( $_SERVER['REMOTE_ADDR'] ) ) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        else {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $ipArr = explode( ',', $ip );
        $ip    = $ipArr[ count( $ipArr ) - 1 ];

        return trim( $ip );
    }

    /**
     * Returns decoded cookie
     *
     * @param string $key
     *
     * @return bool|string
     */
    public function getCookie( $key, $decode = false ) {
        $cookie = Mage::getModel( 'core/cookie' );
        if ( $cookie->get( $key ) ) {
            $result = $cookie->get( $key );
            if ( $decode ) {
                $result = base64_decode( $result );
            }

            return $result;
        }
        else {
            return false;
        }
    }

    /**
     * Sets encoded cookie
     *
     * @param string $key
     * @param mixed $value
     *
     * @return Zend_Controller_Request_Http
     */
    public function setCookie( $key, $value, $encode = true ) {
        $cookie = Mage::getModel( 'core/cookie' );
        if ( $encode ) {
            $value = base64_encode( $value );
        }

        foreach ( Mage::app()->getStores() as $store ) {
            $urlParse = parse_url( $store->getBaseUrl() );
            $path     = rtrim( str_replace( 'index.php', '', $urlParse['path'] ), '/' );

            $cookie->set( $key, $value, true, $path );
        }

        return true;
    }
}
